//
// Created by WydD on 28/05/2018.
//

#include "bcm_binding.h"


static PyGetSetDef PyBCMCharge_getset[] = {
        PyGetterDef(PyBCMCharge, direction, PyLong_FromLong, int),
        PyGetterDef(PyBCMCharge, properties, PyLong_FromLong, int),
        PyGetterDef(PyBCMCharge, frames, PyLong_FromLong, int),
        PyGetterDef(PyBCMCharge, flags, PyLong_FromLong, int),
        PyGetterDef(PyBCMCharge, index, PyLong_FromLong, int),
        PyGetterDef(PyBCMCharge, uk2, PyLong_FromLong, int),
        PyGetterDef(PyBCMCharge, uk3, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBCMChargeType = {
        TypeDef_HEAD(Charge, PyBCMCharge),
        genericFree,
        TypeDef(
                nullptr,
                PyBCMCharge_getset,
                genericInit
        )
};

static PyGetSetDef PyBCMMove_getset[] = {
        PyGetterDef(PyBCMMove, index, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, name, PyString_FromStdString, string),
        PyGetterDef(PyBCMMove, input, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, input_flags, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, input_motion_index, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, script_index, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, position_restriction, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, restriction_distance, PyFloat32_FromFloat, float),
        PyGetterDef(PyBCMMove, projectile_limit, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, projectile_group, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, bloody_garden, PyBool_FromLong, bool),
        PyGetterDef(PyBCMMove, category, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, subcategories, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, stance, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, ex_requires, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, ex_consumes, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, vmeter_requires, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, vmeter_consumes, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, vtrigger_activated, PyBool_FromLong, bool),
        PyGetterDef(PyBCMMove, vtrigger_consumes, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, specific_vtrigger_needed, PyBool_FromLong, bool),
        PyGetterDef(PyBCMMove, specific_vtrigger_selected, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown3, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown7, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown9, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown10, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown11, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown17, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown18, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown19, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown20, PyFloat32_FromFloat, float),
        PyGetterDef(PyBCMMove, unknown21, PyFloat32_FromFloat, float),
        PyGetterDef(PyBCMMove, unknown22, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown23, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown24, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown25, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown26, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown27, PyLong_FromLong, int),
        PyGetterDef(PyBCMMove, unknown28, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBCMMoveType = {
        TypeDef_HEAD(Move, PyBCMMove),
        genericFree,
        TypeDef(
                nullptr,
                PyBCMMove_getset,
                genericInit
        )
};

static PyGetSetDef PyBCMInputPart_getset[] = {
        PyGetterDef(PyBCMInputPart, type, PyLong_FromLong, int),
        PyGetterDef(PyBCMInputPart, buffer, PyLong_FromLong, int),
        PyGetterDef(PyBCMInputPart, direction, PyLong_FromLong, int),
        PyGetterDef(PyBCMInputPart, properties, PyLong_FromLong, int),
        PyGetterDef(PyBCMInputPart, uk2, PyLong_FromLong, int),
        PyGetterDef(PyBCMInputPart, uk3, PyLong_FromLong, int),
        PyGetterDef(PyBCMInputPart, uk4, PyLong_FromLong, int),
        PyGetterDef(PyBCMInputPart, uk5, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBCMInputPartType = {
        TypeDef_HEAD(InputPart, PyBCMInputPart),
        genericFree,
        TypeDef(
                nullptr,
                PyBCMInputPart_getset,
                genericInit
        )
};

static PyGetSetDef PyBCMCancelListEntry_getset[] = {
        PyGetterDef(PyBCMCancelListEntry, move_id, PyLong_FromLong, int),
        {"unknown_bytes", [](PyObject *_self, void *) {
                return convertContainerToList(((PyBCMCancelListEntry *) _self)->ref->unknown_bytes, PyLong_FromLong);
        }, nullptr, ":type: list[int]"},
        {"optional_unknown_ints", [](PyObject *_self, void *) {
            auto &uk_int = ((PyBCMCancelListEntry *) _self)->ref->optional_unknown_ints;
            if (uk_int) {
                return convertContainerToList(*uk_int, PyLong_FromLong);
            } else {
                Py_INCREF(Py_None);
                return Py_None;
            }
        }, nullptr, ":type: list[int]"},
        PySentinel
};

PyTypeObject PyBCMCancelListEntryType = {
        TypeDef_HEAD(CancelListEntry, PyBCMCancelListEntry),
        genericFree,
        TypeDef(
                nullptr,
                PyBCMCancelListEntry_getset,
                genericInit
        )
};

static PyGetSetDef PyBCMCancelList_getset[] = {
        PyGetterDef(PyBCMCancelList, unknown1, PyLong_FromLong, int),
        PyDirectGetterDef(PyBCMCancelList, moves, list[sfvfile_exp.CancelListEntry]),
        PySentinel
};

PyTypeObject PyBCMCancelListType = {
        TypeDef_HEAD(CancelList, PyBCMCancelList),
        [](PyObject* _self) {
            auto self = (PyBCMCancelList*)_self;
            Py_XDECREF(self->moves);
            genericFree(_self);
        },
        TypeDef(
                nullptr,
                PyBCMCancelList_getset,
                [](PyObject* _self, PyObject*, PyObject*) -> int {
                    auto self = (PyBCMCancelList*)_self;
                    self->moves = convertContainerToList(
                            self->ref->moves,
                            [](auto &entry) { return rawObjAsPythonType(entry, &PyBCMCancelListEntryType); }
                    );
                    return 0;
                }
        )
};

int init_bcm(PyBCM *self, PyObject *args, PyObject *kwds) {
    PyObject* object;
    if (!PyArg_ParseTuple(args, "O", &object)) {
        return -1;
    };
    self->bcm = load_file<BCM>(object);

    self->moves = convertContainerToList(
            self->bcm->moves,
            [](auto &move) {
                return rawObjAsPythonType(move, &PyBCMMoveType);
            }
    );

    self->inputs = convertContainerToList(
            self->bcm->inputs,
            [](auto &inputs_list) {
                return convertContainerToList(inputs_list, [](auto &inputs) {
                    return convertContainerToList(inputs, [](auto &input_part) {
                        return rawObjAsPythonType(input_part, &PyBCMInputPartType);
                    });
                });
            }
    );

    self->charges = convertContainerToList(
            self->bcm->charges,
            [](auto &charge) {
                return rawObjAsPythonType(charge, &PyBCMChargeType);
            }
    );


    self->cancel_lists = convertContainerToList(
            self->bcm->cancel_lists,
            [](auto &cl) {
                return ptrAsPythonType(cl, &PyBCMCancelListType);
            }
    );

    return 0;
}


static PyGetSetDef PyBCM_getset[] = {
        PyDirectGetterDef(PyBCM, charges, list[sfvfile_exp.Charge]),
        PyDirectGetterDef(PyBCM, moves, list[sfvfile_exp.Move]),
        PyDirectGetterDef(PyBCM, inputs, list[list[list[sfvfile_exp.InputPart]]]),
        PyDirectGetterDef(PyBCM, cancel_lists, list[sfvfile_exp.CancelList]),
        PySentinel
};

PyTypeObject PyBCMType = {
        TypeDef_HEAD(CancelList, PyBCM),
        [](PyObject* _self) {
            auto self = (PyBCM*)_self;
            Py_XDECREF(self->moves);
            Py_XDECREF(self->charges);
            Py_XDECREF(self->inputs);
            Py_XDECREF(self->cancel_lists);
            delete self->bcm;
            genericFree(_self);
        },
        TypeDef(
                nullptr,
                PyBCM_getset,
                init_bcm
        )
};
