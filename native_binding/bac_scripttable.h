//
// Created by WydD on 21/05/2018.
//

#ifndef SFVSIM_NATIVE_BAC_SCRIPTTABLE_H
#define SFVSIM_NATIVE_BAC_SCRIPTTABLE_H

#include <Python.h>
#include <sfvfile/bac.h>

typedef struct {
    PyObject_HEAD // no semicolon
    ScriptTable *ref;
    PyObject *scripts;
} PyBACScriptTable;

extern PyTypeObject PyBACScriptTableType;

#endif //SFVSIM_NATIVE_BAC_SCRIPTTABLE_H
