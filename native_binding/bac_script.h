//
// Created by WydD on 21/05/2018.
//

#ifndef SFVSIM_NATIVE_BAC_BINDING_SCRIPT_H
#define SFVSIM_NATIVE_BAC_BINDING_SCRIPT_H

// Python includes
#include <Python.h>
#include <sfvfile/bac.h>
#include "binding_util.h"

typedef PyRefBase<CommandList> PyBACCommandList;
extern PyTypeObject PyBACCommandListType;

typedef PyRefBase<Script> PyBACScript;
extern PyTypeObject PyBACScriptType;

typedef PyRefBase<EffectHeader> PyBACEffectHeader;
extern PyTypeObject PyBACEffectHeaderType;

#endif //SFVSIM_NATIVE_BAC_BINDING_SCRIPT_H
