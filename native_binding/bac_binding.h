//
// Created by WydD on 28/05/2018.
//

#ifndef SFVSIM_NATIVE_BAC_BINDING_H
#define SFVSIM_NATIVE_BAC_BINDING_H

#include "binding_util.h"

#include "bac_script.h"
#include "bac_scripttable.h"
#include "bac_hitbox_effect.h"
#include "bac_timeline.h"

typedef struct {
    PyObject_HEAD // no semicolon
    BAC *bac;
    PyObject *script_tables;
    PyObject *hitbox_effects;
} PyBAC;

extern PyTypeObject PyBACType;

#endif //SFVSIM_NATIVE_BAC_BINDING_H
