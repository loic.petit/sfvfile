//
// Created by WydD on 28/05/2018.
//

#ifndef SFVSIM_NATIVE_BCM_BINDING_H
#define SFVSIM_NATIVE_BCM_BINDING_H


#include <sfvfile/bcm.h>
#include "binding_util.h"

typedef PyRefBase<Charge> PyBCMCharge;
extern PyTypeObject PyBCMChargeType;

typedef PyRefBase<Move> PyBCMMove;
extern PyTypeObject PyBCMMoveType;

typedef PyRefBase<InputPart> PyBCMInputPart;
extern PyTypeObject PyBCMInputPartType;

typedef PyRefBase<CancelListEntry> PyBCMCancelListEntry;
extern PyTypeObject PyBCMCancelListEntryType;

typedef struct {
    PyObject_HEAD
    CancelList *ref;
    PyObject* moves;
} PyBCMCancelList;
extern PyTypeObject PyBCMCancelListType;

typedef struct {
    PyObject_HEAD
    BCM *bcm;
    PyObject* charges;
    PyObject* moves;
    PyObject* inputs;
    PyObject* cancel_lists;
} PyBCM;
extern PyTypeObject PyBCMType;

#endif //SFVSIM_NATIVE_BCM_BINDING_H
