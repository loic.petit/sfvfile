
#undef DEBUG
#define HAVE_SNPRINTF

// Python includes
#include <Python.h>
#include <structmember.h>
#include <iostream>
#include <stdio.h>


// STD includes
#include <fstream>
#include <sfvfile/uasset.h>

#include "binding_util.h"

#include "bac_script.h"
#include "bac_scripttable.h"
#include "bac_hitbox_effect.h"
#include "bac_timeline.h"
#include "bac_binding.h"
#include "bcm_binding.h"
#include "bch_binding.h"

#include <sfvfile/json.hpp>

using json = nlohmann::json;

PyObject *convertJsonToPython(json &data) {
    if (data.is_boolean()) {
        return PyBool_FromLong(data.get<bool>());
    }
    if (data.is_string()) {
        return PyString_FromStdString(data.get<std::string>());
    }
    if (data.is_number_integer()) {
        return PyLong_FromLong(data.get<int64_t>());
    }
    if (data.is_number_unsigned()) {
        return PyLong_FromLong(data.get<uint64_t>());
    }
    if (data.is_number_float()) {
        return PyFloat_FromDouble(data.get<double>());
    }
    if (data.is_object()) {
        PyObject *res = PyDict_New();
        for (json::iterator it = data.begin(); it != data.end(); ++it) {
            PyObject *item = convertJsonToPython(it.value());
            PyDict_SetItemString(res, it.key().c_str(), item);
            Py_DECREF(item);
        }
        return res;
    }
    if (data.is_array()) {
        PyObject* res = PyList_New(data.size());
        int i = 0;
        for (auto& element : data) {
            PyList_SET_ITEM(res, i++, convertJsonToPython(element));
        }
        return res;
    }
    Py_RETURN_NONE;
}

PyObject* uasset_load(PyObject *self, PyObject *obj) {
    Uasset uasset;
    if (PyUnicode_CheckExact(obj)) {
        const char *name = PyUnicode_AsUTF8(obj);
        std::ifstream f(name, std::ios::binary | std::ios::in);
        if (!f.good()) {
            throw ParseError("Unable to load file " + (std::string(name)) + " does it exist?");
        }
        f >> uasset;
    } else {
        byte_array_buffer stream = {(uint8_t*)PyBytes_AsString(obj), (size_t)(PyBytes_GET_SIZE(obj))};
        std::istream in(&stream);
        in >> uasset;
    }
    return convertJsonToPython(uasset.data);
}

static PyMethodDef module_methods[] = {
        {"Uasset", uasset_load, METH_O, nullptr},
        { nullptr }
};

static struct PyModuleDef module_def = {
        PyModuleDef_HEAD_INIT,
        ModuleName,
        "sfvfile module",
        -1,
        module_methods
};

PyMODINIT_FUNC PyInit_sfvfile(void) {

    PyObject *m;

    m = PyModule_Create(&module_def);
    if (m == nullptr)
        return nullptr;

    if (
            addTypeToModule(m, "BAC", PyBACType) &&
            addTypeToModule(m, "ScriptTable", PyBACScriptTableType) &&
            addTypeToModule(m, "HitboxEffect", PyBACHitboxEffectType) &&
            addTypeToModule(m, "Script", PyBACScriptType) &&
            addTypeToModule(m, "EffectHeader", PyBACEffectHeaderType) &&
            addTypeToModule(m, "TimeElement", PyBACTimeElementType) &&
            addTypeToModule(m, "Box", PyBACBoxType) &&
            addTypeToModule(m, "Force", PyBACForceType) &&
            addTypeToModule(m, "AutoCancel", PyBACAutoCancelType) &&
            addTypeToModule(m, "Status", PyBACStatusType) &&
            addTypeToModule(m, "Cancel", PyBACCancelType) &&
            addTypeToModule(m, "Command", PyBACCommandType) &&
            addTypeToModule(m, "Hitbox", PyBACHitboxType) &&
            addTypeToModule(m, "Hurtbox", PyBACHurtboxType) &&
            addTypeToModule(m, "Pushbox", PyBACPushboxType) &&
            addTypeToModule(m, "CommandList", PyBACCommandListType) &&
            addTypeToModule(m, "BCM", PyBCMType) &&
            addTypeToModule(m, "Charge", PyBCMChargeType) &&
            addTypeToModule(m, "Move", PyBCMMoveType) &&
            addTypeToModule(m, "InputPart", PyBCMInputPartType) &&
            addTypeToModule(m, "CancelListEntry", PyBCMCancelListEntryType) &&
            addTypeToModule(m, "CancelList", PyBCMCancelListType) &&
            addTypeToModule(m, "BCH", PyBCHType) &&
            true
            ) {
        return m;
    }

    Py_XDECREF(m);
    return nullptr;
}
