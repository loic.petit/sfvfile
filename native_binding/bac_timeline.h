//
// Created by WydD on 27/05/2018.
//

#ifndef SFVSIM_NATIVE_BAC_TIMELINE_H
#define SFVSIM_NATIVE_BAC_TIMELINE_H

#include <Python.h>
#include <sfvfile/bac.h>
#include "binding_util.h"

typedef PyRefBase<TimeElement> PyBACTimeElement;
extern PyTypeObject PyBACTimeElementType;

typedef PyRefBase<Box> PyBACBox;
extern PyTypeObject PyBACBoxType;

typedef PyRefBase<AutoCancel> PyBACAutoCancel;
extern PyTypeObject PyBACAutoCancelType;

typedef PyRefBase<Status> PyBACStatus;
extern PyTypeObject PyBACStatusType;

typedef PyRefBase<Force> PyBACForce;
extern PyTypeObject PyBACForceType;

typedef PyRefBase<Cancel> PyBACCancel;
extern PyTypeObject PyBACCancelType;

typedef PyRefBase<Command> PyBACCommand;
extern PyTypeObject PyBACCommandType;

typedef PyReleaseRefBase<Hitbox> PyBACHitbox;
extern PyTypeObject PyBACHitboxType;

typedef PyReleaseRefBase<Hurtbox> PyBACHurtbox;
extern PyTypeObject PyBACHurtboxType;

typedef PyReleaseRefBase<Pushbox> PyBACPushbox;
extern PyTypeObject PyBACPushboxType;

#endif //SFVSIM_NATIVE_BAC_TIMELINE_H
