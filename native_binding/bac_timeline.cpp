//
// Created by WydD on 27/05/2018.
//

#include "bac_timeline.h"
#include "binding_util.h"

#define ParameterDef(Struct) \
    {"parameters", [](PyObject *_self, void *) { \
        return convertContainerToList(((Struct *) _self)->ref->parameters, PyLong_FromLong); \
    }, nullptr, ":type: list[int]"}

static PyGetSetDef PyBACTimeElement_getset[] = {
        PyGetterDef(PyBACTimeElement, tick_start, PyLong_FromLong, int),
        PyGetterDef(PyBACTimeElement, tick_end, PyLong_FromLong, int),
        PySentinel
};

static PyMethodDef PyBACTimeElement_methods[] = {
        {"asdict", [](PyObject* _self, PyObject *other) {
            auto self = (PyBACBox*) _self;
            PyObject *result = PyDict_New();
            PyTypeObject *type = Py_TYPE(self);
            while (type != nullptr) {
                PyGetSetDef *def = type->tp_getset;
                if (def) {
                    while (def->name != nullptr) {
                        if (def->name[0] != '_') {
                            PyObject *value = def->get(_self, nullptr);
                            if (Py_TYPE(value) == npflt) {
                                PyObject *realFloat = PyFloat_FromDouble(((PyFloatScalarObject*)value)->obval);
                                Py_DECREF(value);
                                value = realFloat;
                            }
                            PyDict_SetItemString(result, def->name, value);
                            Py_DECREF(value);
                        }
                        ++def;
                    }
                }
                type = type->tp_base;
            }
            return result;
        }, METH_NOARGS, ":rtype: bool"},
        PySentinel
};

PyTypeObject PyBACTimeElementType = {
        TypeDef_HEAD(TimeElement, PyBACTimeElement),
        genericFree,
        TypeDef(
                PyBACTimeElement_methods,
                PyBACTimeElement_getset,
                genericInit
        )
};


static PyMethodDef PyBACBox_methods[] = {
        {"does_collide", [](PyObject* _self, PyObject *other) {
            auto self = (PyBACBox*) _self;
            return PyBool_FromLong(self->ref->does_collide(*((PyBACBox*) other)->ref));
        }, METH_O, ":rtype: bool"},
        {"x_collision_distance", [](PyObject* _self, PyObject *args) -> PyObject* {
            auto self = (PyBACBox*) _self;
            PyObject *other;
            bool side;
            if (!PyArg_ParseTuple(args, "Ob", &other, &side)) {
                return nullptr;
            }
            return PyFloat32_FromFloat(self->ref->x_collision_distance(*((PyBACBox*) other)->ref, side));
        }, METH_VARARGS, ":rtype: numpy.float32"},
        PySentinel
};

static PyGetSetDef PyBACBox_getset[] = {
        PyGetterDef(PyBACBox, x, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACBox, y, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACBox, z, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACBox, width, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACBox, height, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACBox, unknown1, PyLong_FromLong, int),
        PyGetterDef(PyBACBox, unknown2, PyLong_FromLong, int),
        PyGetterDef(PyBACBox, unknown3, PyLong_FromLong, int),
        PyGetterDef(PyBACBox, unknown4, PyLong_FromLong, int),
        PyGetterDef(PyBACBox, unknown5, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACBoxType = {
        TypeDef_HEAD(Box, PyBACBox),
        genericFree,
        TypeDefExt(
                PyBACTimeElementType,
                PyBACBox_methods,
                PyBACBox_getset,
                genericInit
        )
};



static PyGetSetDef PyBACAutoCancel_getset[] = {
        PyGetterDef(PyBACAutoCancel, condition, PyLong_FromLong, int),
        PyGetterDef(PyBACAutoCancel, script_index, PyLong_FromLong, int),
        PyGetterDef(PyBACAutoCancel, script_time, PyLong_FromLong, int),
        PyGetterDef(PyBACAutoCancel, execution_flag, PyLong_FromLong, int),
        PyGetterDef(PyBACAutoCancel, state_change, PyLong_FromLong, int),
        PyGetterDef(PyBACAutoCancel, unknown3, PyLong_FromLong, int),
        PyGetterDef(PyBACAutoCancel, unknown4, PyLong_FromLong, int),
        ParameterDef(PyBACAutoCancel),
        PySentinel
};

PyTypeObject PyBACAutoCancelType = {
        TypeDef_HEAD(AutoCancel, PyBACAutoCancel),
        genericFree,
        TypeDefExt(
                PyBACTimeElementType,
                nullptr,
                PyBACAutoCancel_getset,
                genericInit
        )
};

static PyGetSetDef PyBACStatus_getset[] = {
        PyGetterDef(PyBACStatus, player_flags, PyLong_FromLong, int),
        PyGetterDef(PyBACStatus, opponent_flags, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACStatusType = {
        TypeDef_HEAD(Status, PyBACStatus),
        genericFree,
        TypeDefExt(
                PyBACTimeElementType,
                nullptr,
                PyBACStatus_getset,
                genericInit
        )
};

static PyGetSetDef PyBACForce_getset[] = {
        PyGetterDef(PyBACForce, amount, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACForce, flags, PyLong_FromLong, int),
        PyGetterDef(PyBACForce, type, PyLong_FromLong, int),
        PyGetterDef(PyBACForce, type_flag, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACForceType = {
        TypeDef_HEAD(Force, PyBACForce),
        genericFree,
        TypeDefExt(
                PyBACTimeElementType,
                nullptr,
                PyBACForce_getset,
                genericInit
        )
};


static PyGetSetDef PyBACCancel_getset[] = {
        PyGetterDef(PyBACCancel, cancel_list, PyLong_FromLong, int),
        PyGetterDef(PyBACCancel, type, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACCancelType = {
        TypeDef_HEAD(Cancel, PyBACCancel),
        genericFree,
        TypeDefExt(
                PyBACTimeElementType,
                nullptr,
                PyBACCancel_getset,
                genericInit
        )
};


static PyGetSetDef PyBACCommand_getset[] = {
        PyGetterDef(PyBACCommand, category, PyLong_FromLong, int),
        PyGetterDef(PyBACCommand, command, PyLong_FromLong, int),
        ParameterDef(PyBACCommand),
        PySentinel
};

PyTypeObject PyBACCommandType = {
        TypeDef_HEAD(Command, PyBACCommand),
        genericFree,
        TypeDefExt(
                PyBACTimeElementType,
                nullptr,
                PyBACCommand_getset,
                genericInit
        )
};

static PyGetSetDef PyBACHitbox_getset[] = {
        PyGetterDef(PyBACHitbox, hit_restriction, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, flags, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, group, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, strength, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, number_of_hits, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, hit_level, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, hit_type, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, juggle_limit, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, juggle_increase, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, unknown_flags, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, effect_index, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, hit_range, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, unknown11, PyLong_FromLong, int),
        PyGetterDef(PyBACHitbox, unknown12, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACHitboxType = {
        TypeDef_HEAD(Hitbox, PyBACHitbox),
        genericReleaseRefFree<Hitbox>,
        TypeDefExt(
                PyBACBoxType,
                nullptr,
                PyBACHitbox_getset,
                genericReleaseRefInit
        )
};

static PyGetSetDef PyBACHurtbox_getset[] = {
        PyGetterDef(PyBACHurtbox, vulnerability_flags, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, unknown7, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, armor_group, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, armor_strength, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, armor_count, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, unknown9, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, hurt_type, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, armor_effect, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, hurt_level, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, armor_freeze, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, unknown11, PyLong_FromLong, int),
        PyGetterDef(PyBACHurtbox, unknown12, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACHurtbox, unknown13, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACHurtboxType = {
        TypeDef_HEAD(Hurtbox, PyBACHurtbox),
        genericReleaseRefFree<Hurtbox>,
        TypeDefExt(
                PyBACBoxType,
                nullptr,
                PyBACHurtbox_getset,
                genericReleaseRefInit
        )
};
static PyMethodDef PyBACPushbox_methods[] = {
        {"apply_correction", [](PyObject* _self, PyObject *correction) -> PyObject* {
            auto self = (PyBACPushbox*) _self;
            float dx;
            if (Py_TYPE(correction) == npflt) {
                dx = ((PyFloatScalarObject *) correction)->obval;
            } else {
                dx = static_cast<float>(PyFloat_AsDouble(correction));
            }
            self->ref->apply_correction(dx);
            Py_RETURN_NONE;
        }, METH_O, ":rtype: void"},
        PySentinel
};


static PyGetSetDef PyBACPushbox_getset[] = {
        PyGetterDef(PyBACPushbox, unknown6, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACPushboxType = {
        TypeDef_HEAD(Pushbox, PyBACPushbox),
        genericReleaseRefFree<Pushbox>,
        TypeDefExt(
                PyBACBoxType,
                PyBACPushbox_methods,
                PyBACPushbox_getset,
                genericReleaseRefInit
        )
};
