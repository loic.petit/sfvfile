//
// Created by WydD on 21/05/2018.
//

#include "binding_util.h"

void genericFree(PyObject *self) {
    Py_TYPE(self)->tp_free(self);
}

int genericReleaseRefInit(PyObject *self, PyObject *args, PyObject *kwargs) {
    ((PyReleaseRefBase<void>*)self)->release = false;
    return 0;
}

int genericInit(PyObject *self, PyObject *args, PyObject *kwargs) {
    return 0;
}

PyTypeObject *npflt = nullptr;

bool addTypeToModule(PyObject *module, const char *name, PyTypeObject &type) {
    if (PyType_Ready(&type) < 0)
        return false;
    if (npflt == nullptr) {
        PyObject *flt = PyObject_CallObject(PyObject_GetAttrString(PyImport_ImportModule("numpy"), "float32"), nullptr);
        npflt = Py_TYPE(flt);
        Py_DECREF(flt);
    }
    PyModule_AddObject(module, name, (PyObject*) &type);
    return true;
}

uint8_t *skip_uasset_header(uint8_t *uasset) {
    uint32_t header_size;
    header_size = *((uint32_t*) (uasset+0x18));
    // header_size + 0x20 is the offset of the (uint32 size, byte[] content) so skip to that
    return uasset + header_size + 0x20 + sizeof(uint32_t);
}

byte_array_buffer::byte_array_buffer(const uint8_t *begin, const size_t size) :
        begin_(begin),
        end_(begin + size),
        current_(begin_)
{
    assert(std::less_equal<const uint8_t *>()(begin_, end_));
}

byte_array_buffer::int_type byte_array_buffer::underflow()
{
    if (current_ == end_)
        return traits_type::eof();

    return traits_type::to_int_type(*current_);
}

byte_array_buffer::int_type byte_array_buffer::uflow()
{
    if (current_ == end_)
        return traits_type::eof();

    return traits_type::to_int_type(*current_++);
}

byte_array_buffer::int_type byte_array_buffer::pbackfail(int_type ch)
{
    if (current_ == begin_ || (ch != traits_type::eof() && ch != current_[-1]))
        return traits_type::eof();

    return traits_type::to_int_type(*--current_);
}

std::streamsize byte_array_buffer::showmanyc()
{
    assert(std::less_equal<const uint8_t *>()(current_, end_));
    return end_ - current_;
}


std::streampos byte_array_buffer::seekoff ( std::streamoff off, std::ios_base::seekdir way,
                                            std::ios_base::openmode which )
{
    if (way == std::ios_base::beg)
    {
        current_ = begin_ + off;
    }
    else if (way == std::ios_base::cur)
    {
        current_ += off;
    }
    else if (way == std::ios_base::end)
    {
        current_ = end_;
    }

    if (current_ < begin_ || current_ > end_)
        return -1;


    return current_ - begin_;
}

std::streampos byte_array_buffer::seekpos ( std::streampos sp,
                                            std::ios_base::openmode which )
{
    current_ = begin_ + sp;

    if (current_ < begin_ || current_ > end_)
        return -1;

    return current_ - begin_;
}