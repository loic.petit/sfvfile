//
// Created by WydD on 21/05/2018.
//

#include "bac_script.h"

#include "bac_timeline.h"
#include "binding_util.h"

#define BACListGetterDef(Attribute, PyType, Doc) \
        {"" #Attribute, [](PyObject *_self, void *) {\
                return convertContainerToList(((PyBACScript *) _self)->ref->Attribute, [](auto &ref) {\
                        return rawObjAsPythonType(ref, &(PyType));\
                });\
        }, nullptr, ":type: list[sfvfile_exp." #Doc "]"}

#define PyListGetterAt(Attribute, PyType, Doc) \
        {"" #Attribute, [](PyObject *_self, PyObject *args) {\
                auto *self = (PyBACScript*)_self; \
                int32_t time, state; \
                if (!PyArg_ParseTuple(args, "ii", &time, &state)) { \
                    Py_INCREF(Py_None); \
                    return Py_None;\
                } \
                auto lst = self->ref->Attribute(time, state); \
                return convertIterableToList(lst, [](auto &ref) { \
                        return objAsPythonType(ref, &(PyType)); \
                }); \
        }, METH_VARARGS, ":type: list[sfvfile_exp." #Doc "]"}

template<typename E>
inline PyObject *get_boxes_at(PyObject* _self, PyObject *args, std::list<std::unique_ptr<E>> (Script::*get_boxes)(int32_t, int32_t, float, float, bool), PyTypeObject &object_type) {
    auto *self = (PyBACScript *) _self;
    int32_t time, state;
    PyObject *dx_object, *dy_object;
    float dx, dy;
    bool side;
    if (!PyArg_ParseTuple(args, "iiOOb", &time, &state, &dx_object, &dy_object, &side)) {
        return nullptr;
    }
    if (Py_TYPE(dx_object) == npflt) {
        dx = ((PyFloatScalarObject *) dx_object)->obval;
    } else {
        dx = static_cast<float>(PyFloat_AsDouble(dx_object));
    }
    if (Py_TYPE(dy_object) == npflt) {
        dy = ((PyFloatScalarObject *) dy_object)->obval;
    } else {
        dy = static_cast<float>(PyFloat_AsDouble(dy_object));
    }
    auto lst = (self->ref->*get_boxes)(time, state, dx, dy, side);
    return convertIterableToList(lst, [&object_type](std::unique_ptr<E> &ref) {
        // release the ownership to transfer it to the python object
        const auto ptr = ref.release();
        const auto result = objAsPythonType(ptr, &object_type);
        ((PyReleaseRefBase<void> *) result)->release = true;
        return result;
    });
}

static PyMethodDef PyBACScript_methods[] = {
        PyListGetterAt(get_auto_cancels_at, PyBACAutoCancelType, AutoCancel),
        PyListGetterAt(get_status_at, PyBACStatusType, Status),
        PyListGetterAt(get_forces_at, PyBACForceType, Force),
        PyListGetterAt(get_cancels_at, PyBACCancelType, Cancel),
        {"get_commands_at", [](PyObject *_self, PyObject *args) -> PyObject* {
            auto *self = (PyBACScript *) _self;
            int32_t time, state;
            if (!PyArg_ParseTuple(args, "ii", &time, &state)) {
                Py_INCREF(Py_None);
                return Py_None;
            }
            auto val = PyObject_New(PyBACCommandList, &PyBACCommandListType);
            val->ref = self->ref->get_commands_at(time, state).release();
            return (PyObject*)val;
        }, METH_VARARGS, ":type: list[sfvfile_exp.Command]"},
        {"get_hitboxes_at", [](PyObject *self, PyObject *args) -> PyObject* {
            return get_boxes_at(self, args, &Script::get_hitboxes_at, PyBACHitboxType);
        }, METH_VARARGS, ":type: list[sfvfile_exp.Hitbox]"},
        {"get_hurtboxes_at", [](PyObject *self, PyObject *args) -> PyObject* {
            return get_boxes_at(self, args, &Script::get_hurtboxes_at, PyBACHurtboxType);
        }, METH_VARARGS, ":type: list[sfvfile_exp.Hurtbox]"},
        {"get_pushboxes_at", [](PyObject *self, PyObject *args) -> PyObject* {
            return get_boxes_at(self, args, &Script::get_pushboxes_at, PyBACPushboxType);
        }, METH_VARARGS, ":type: list[sfvfile_exp.Pushbox]"},
        //inline std::optional<float> get_position_shift_at(uint32_t category, int32_t time) {
        {"get_position_shift_at", [](PyObject *_self, PyObject *args) -> PyObject* {
            auto *self = (PyBACScript *) _self;
            uint32_t category;
            uint32_t time;
            uint32_t state;
            if (!PyArg_ParseTuple(args, "III", &state, &category, &time)) {
                Py_INCREF(Py_None);
                return Py_None;
            }
            auto result = self->ref->get_position_shift_at(state, category, time);
            if (result) {
                return PyFloat32_FromFloat(*result);
            }
            Py_INCREF(Py_None);
            return Py_None;
        }, METH_VARARGS, ":type: list[sfvfile_exp.Pushbox]"},
        PySentinel
};


static PyGetSetDef PyBACScript_getset[] = {
        PyGetterDef(PyBACScript, index, PyLong_FromLong, int),
        PyGetterDef(PyBACScript, name, PyUnicode_FromStdString, str),
        PyGetterDef(PyBACScript, first_hitbox_frame, PyLong_FromLong, int),
        PyGetterDef(PyBACScript, last_action_frame, PyLong_FromLong, int),
        PyGetterDef(PyBACScript, interrupt_frame, PyLong_FromLong, int),
        PyGetterDef(PyBACScript, total_ticks, PyLong_FromLong, int),
        PyGetterDef(PyBACScript, execution_flags, PyLong_FromLong, int),
        PyGetterDef(PyBACScript, slide_vx, PyFloat32_FromFloat, int),
        PyGetterDef(PyBACScript, slide_vy, PyFloat32_FromFloat, int),
        PyGetterDef(PyBACScript, slide_vz, PyFloat32_FromFloat, int),
        PyGetterDef(PyBACScript, slide_ax, PyFloat32_FromFloat, int),
        PyGetterDef(PyBACScript, slide_ay, PyFloat32_FromFloat, int),
        PyGetterDef(PyBACScript, slide_az, PyFloat32_FromFloat, int),
        PyGetterDef(PyBACScript, flags, PyLong_FromLong, int),
        PyGetterDef(PyBACScript, type_offset, PyLong_FromLong, int),
        {"effect_header", [](PyObject *_self, void *) {
            return ptrAsPythonType(((PyBACScript *) _self)->ref->effect_header, &PyBACEffectHeaderType);
        }, nullptr, ":type: sfvfile_exp.EffectHeader"},
        BACListGetterDef(auto_cancels, PyBACAutoCancelType, AutoCancel),
        BACListGetterDef(status, PyBACStatusType, Status),
        BACListGetterDef(forces, PyBACForceType, Force),
        BACListGetterDef(cancels, PyBACCancelType, Cancel),
        BACListGetterDef(commands, PyBACCommandType, Command),
        BACListGetterDef(hitboxes, PyBACHitboxType, Hitbox),
        BACListGetterDef(hurtboxes, PyBACHurtboxType, Hurtbox),
        BACListGetterDef(pushboxes, PyBACPushboxType, Pushbox),
        PySentinel
};

PyTypeObject PyBACScriptType = {
        TypeDef_HEAD(Script, PyBACScript),
        [](PyObject *_self) {
            auto self = (PyBACScript *) _self;
            Py_TYPE(self)->tp_free(self);
        },
        TypeDef(
                PyBACScript_methods,
                PyBACScript_getset,
                genericInit
        )
};

static PyGetSetDef PyBACEffectHeader_getset[] = {
        PyGetterDef(PyBACEffectHeader, unknown12, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown13, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown14, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown15, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown16, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown17, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown18, PyFloat32_FromFloat, float),
        PyGetterDef(PyBACEffectHeader, unknown19, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown20, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown21, PyLong_FromLong, int),
        PyGetterDef(PyBACEffectHeader, unknown22, PyLong_FromLong, int),
        PySentinel
};

PyTypeObject PyBACEffectHeaderType = {
        TypeDef_HEAD(EffectHeader, PyBACEffectHeader),
        genericFree,
        TypeDef(
                nullptr,
                PyBACEffectHeader_getset,
                genericInit
        )
};


static PyMethodDef PyBACCommandList_methods[] = {
        {"find_all", [](PyObject *_self, PyObject *args) -> PyObject* {
            auto *self = (PyBACCommandList *) _self;
            uint32_t category;
            uint16_t command;
            if (!PyArg_ParseTuple(args, "IH", &category, &command)) {
                return nullptr;
            }
            auto lst = self->ref->get(category, command);
            return convertIterableToList(lst, [](auto &ref) { return objAsPythonType(ref, &(PyBACCommandType)); });
        }, METH_VARARGS, ":type: list[sfvfile_exp.Command]"},
        {"find_one", [](PyObject *_self, PyObject *args) -> PyObject* {
            auto *self = (PyBACCommandList *) _self;
            uint32_t category;
            uint16_t command;
            if (!PyArg_ParseTuple(args, "IH", &category, &command)) {
                return nullptr;
            }
            return objAsPythonType(self->ref->find_one(category, command), &(PyBACCommandType));
        }, METH_VARARGS, ":type: sfvfile_exp.Command"},
        {"size", [](PyObject *_self, PyObject*) -> PyObject* {
            auto *self = (PyBACCommandList *) _self;
            return PyLong_FromSize_t(self->ref->size());
        }, METH_NOARGS, ":type: int"},
        {"contains", [](PyObject *_self, PyObject *args) -> PyObject* {
            auto *self = (PyBACCommandList *) _self;
            uint32_t category;
            uint16_t command;
            if (!PyArg_ParseTuple(args, "IH", &category, &command)) {
                return nullptr;
            }
            return PyBool_FromLong(self->ref->contains(category, command));
        }, METH_VARARGS, ":type: bool"},
        PySentinel
};

PyTypeObject PyBACCommandListType = {
        TypeDef_HEAD(CommandList, PyBACCommandList),
        [](PyObject *_self) {
            auto *self = (PyBACCommandList *) _self;
            delete self->ref;
            self->ref = nullptr;
            Py_TYPE(self)->tp_free(self);
        },
        TypeDef(
                PyBACCommandList_methods,
                nullptr,
                [](PyObject *self, PyObject *args, PyObject *kwargs) {
                    ((PyBACCommandList *) self)->ref = nullptr;
                    return 0;
                }
        )
};
