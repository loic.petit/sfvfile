//
// Created by WydD on 21/05/2018.
//

#ifndef SFVSIM_NATIVE_BAC_BINDING_HITBOX_EFFECT_H
#define SFVSIM_NATIVE_BAC_BINDING_HITBOX_EFFECT_H

// Python includes
#include <Python.h>
#include <sfvfile/bac.h>


typedef struct {
    PyObject_HEAD
    HitboxEffect *ref;
} PyBACHitboxEffect;

extern PyTypeObject PyBACHitboxEffectType;

#endif //SFVSIM_NATIVE_BAC_BINDING_HITBOX_EFFECT_H
