//
// Created by WydD on 29/05/2018.
//

#ifndef SFVSIM_NATIVE_BCH_BINDING_H
#define SFVSIM_NATIVE_BCH_BINDING_H


#include <sfvfile/bch.h>
#include "binding_util.h"

typedef PyRefBase<BCH> PyBCH;
extern PyTypeObject PyBCHType;


#endif //SFVSIM_NATIVE_BCH_BINDING_H
