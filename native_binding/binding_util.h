//
// Created by WydD on 21/05/2018.
//

#ifndef SFVSIM_NATIVE_MACROS_H
#define SFVSIM_NATIVE_MACROS_H

#pragma GCC diagnostic ignored "-Wwrite-strings"

#include <Python.h>
#include <structmember.h>
#include <fstream>
#include <sfvfile/util.h>

#define PyIncLambda(Type, args, act) [](PyObject* _self, args) { auto *self = (Type*)_self; auto result = act; Py_XINCREF(result); return result; }
#define PyLambda(Type, args, act) [](PyObject* _self, args) { auto *self = (Type*)_self; act; }
#define PyGetter(Type, Attribute, Convert) PyLambda(Type, void*, return Convert(self->ref->Attribute))
#define PyGetterDef(Type, Attribute, Convert, DocType) {"" #Attribute, PyGetter(Type, Attribute, Convert), nullptr, ":type: " #DocType }
#define PyDirectGetterDef(Type, Attribute, DocType) {"" #Attribute, PyIncLambda(Type, void*, self->Attribute), nullptr, ":type: " #DocType }

#define ModuleName "sfvfile_exp"
#define TypeDef_HEAD(Type, PyType) PyVarObject_HEAD_INIT(nullptr, 0) "sfvfile_exp."#Type, sizeof(PyType), 0
#define TypeDef_1 0, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* doc */ nullptr, nullptr, nullptr, nullptr, 0, nullptr, nullptr
#define TypeDef_2 nullptr, nullptr, nullptr, 0
#define TypeDef_TAIL nullptr, (newfunc) PyType_GenericNew
#define TypeDefExt(Ext, Methods, GetSet, Init) TypeDef_1, Methods, nullptr, GetSet, &(Ext), TypeDef_2, (initproc) (Init), TypeDef_TAIL
#define TypeDef(Methods, GetSet, Init) TypeDef_1, Methods, nullptr, GetSet, nullptr, TypeDef_2, (initproc) (Init), TypeDef_TAIL
#define PySentinel {nullptr}

template<class Type>
struct PyRefBase {
    PyObject_HEAD
    Type *ref;
};

template<class Type>
struct PyReleaseRefBase {
    PyObject_HEAD
    Type *ref;
    bool release;
};

template<typename Input>
PyObject* objAsPythonType(Input* in, PyTypeObject* typeObject) {
    if (in == nullptr) { Py_INCREF(Py_None); return Py_None; }
    auto val = PyObject_New(PyRefBase<Input>, typeObject);
    val->ref = in;
    typeObject->tp_init((PyObject *) val, nullptr, nullptr);
    return (PyObject *) val;
}

template<typename Input>
PyObject* rawObjAsPythonType(Input &in, PyTypeObject *typeObject) {
    return objAsPythonType(&in, typeObject);
}

template<typename Input>
PyObject* ptrAsPythonType(Input &in, PyTypeObject* typeObject) {
    auto ptr = in.get();
    return objAsPythonType(ptr, typeObject);
}

template<typename Container, typename Func>
inline PyObject* convertContainerToList(Container &ref, Func apply) {
    const Py_ssize_t length = ref.size();
    const auto lst = PyList_New(length);
    for (auto i = 0; i < length; ++i) {
        PyList_SET_ITEM(lst, i, (PyObject *) apply(ref[i]));
    }
    return lst;
}

template<typename Container, typename Func>
inline PyObject* convertIterableToList(Container &ref, Func apply) {
    const Py_ssize_t length = ref.size();
    const auto lst = PyList_New(length);
    auto i = 0;
    for (auto &elem : ref) {
        PyList_SET_ITEM(lst, i++, (PyObject *) apply(elem));
    }
    return lst;
}
#include <cstdio>
#include <string>
#include <list>
#include <fstream>
#include <iostream>

class byte_array_buffer : public std::streambuf
{
public:
    byte_array_buffer(const uint8_t *begin, const size_t size);

private:
    int_type underflow();
    int_type uflow();
    int_type pbackfail(int_type ch);
    std::streamsize showmanyc();
    std::streampos seekoff ( std::streamoff off, std::ios_base::seekdir way,
                             std::ios_base::openmode which = std::ios_base::in | std::ios_base::out );
    std::streampos seekpos ( std::streampos sp,
                             std::ios_base::openmode which = std::ios_base::in | std::ios_base::out);

    // copy ctor and assignment not implemented;
    // copying not allowed
    byte_array_buffer(const byte_array_buffer &);
    byte_array_buffer &operator= (const byte_array_buffer &);

private:
    const uint8_t * const begin_;
    const uint8_t * const end_;
    const uint8_t * current_;
};

/**
 * From a in-memory file, skip the uasset header
 *
 * @param uasset the uasset file
 * @return the pointer to the uasset content
 */
uint8_t* skip_uasset_header(uint8_t* uasset);

template<class T>
T *load_file(PyObject* &obj) {
    auto ptr = new T();
    if (PyUnicode_CheckExact(obj)) {
        const char *name = PyUnicode_AsUTF8(obj);
        std::ifstream f(name, std::ios::binary | std::ios::in);
        if (!f.good()) {
            throw ParseError("Unable to load file " + (std::string(name)) + " does it exist?");
        }
        f >> *ptr;
    } else {
        uint8_t *p = skip_uasset_header((uint8_t*)(PyBytes_AsString(obj)));
        ptr->load_data(p);
    }
    return ptr;
}

void genericFree(PyObject *self);

int genericReleaseRefInit(PyObject *self, PyObject *args, PyObject *kwargs);

template<typename E>
void genericReleaseRefFree(PyObject *_self) {
    auto self = ((PyReleaseRefBase<E>*)_self);
    if (self->release && self->ref) {
        delete self->ref;
        self->ref = nullptr;
    }
    genericFree(_self);
}

int genericInit(PyObject *self, PyObject *args, PyObject *kwargs);

bool addTypeToModule(PyObject* module, const char * name, PyTypeObject& type);

inline PyObject* PyUnicode_FromStdString(std::string& str) {
    return PyUnicode_FromString(str.c_str());
}

extern _typeobject *npflt;

typedef struct {
    PyObject_HEAD
    float obval;
} PyFloatScalarObject;

inline PyObject* PyFloat32_FromFloat(float value) {
    PyFloatScalarObject *const scalar = PyObject_New(PyFloatScalarObject, npflt);
    scalar->obval = value;
    return (PyObject *) scalar;
}

inline PyObject* PyString_FromStdString(const std::string &value) {
    return PyUnicode_FromString(value.c_str());
}

#endif //SFVSIM_NATIVE_MACROS_H
