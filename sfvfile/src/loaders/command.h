//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_COMMAND_H
#define SFVSIM_NATIVE_LOADER_COMMAND_H

#include "../../include/sfvfile/bac.h"
#include <cstring>

struct command_struct {
    uint32_t category;
    uint16_t command;
    uint16_t number_of_parameters;
    uint16_t parameter_ptr;
};

void load_command(Script &script, tick_header& tick, tick_flags& tf, command_struct& elt) {
    auto &command = script.commands.emplace_back(
        tick.tick_start, tick.tick_end, tf, elt.category, elt.command, elt.number_of_parameters
    );

    if (elt.number_of_parameters > 0) {
        memcpy(command.parameters.data(), ((uint8_t*) &elt)+elt.parameter_ptr, elt.number_of_parameters * sizeof(uint16_t));
    }
}

#endif //SFVSIM_NATIVE_LOADER_COMMAND_H
