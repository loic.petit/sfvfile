//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_POSITION_H
#define SFVSIM_NATIVE_LOADER_POSITION_H

#include <algorithm>
#include "box.h"
#include "../../include/sfvfile/bac.h"

struct position_base {
    float value;
    uint32_t flag;
};

void load_position(Script &script, tick_header &tick_header, tick_flags& tf, position_base &elt) {
    Position *position = &(*script.positions.try_emplace(elt.flag, script.total_ticks).first).second;
    if (tf.element_active_on_states != 0 || tf.element_inactive_on_states != 0) {
        uint64_t conditional_key = (static_cast<uint64_t>(static_cast<uint32_t>(tf.element_active_on_states)) << 32) | tf.element_inactive_on_states;
        auto find = position->conditional_positions.find(conditional_key);
        if (find == position->conditional_positions.end()) {
            position = position->conditional_positions.emplace(conditional_key, std::make_unique<Position>(script.total_ticks)).first->second.get();
        } else {
            position = find->second.get();
        }
    }
    std::vector<float> &values = position->values;
    for (int32_t i = tick_header.tick_start ; i < tick_header.tick_end ; ++i) {
        if (std::isnan(values[i]))
            values[i] = elt.value;
        else
            values[i] += elt.value;
    }
}

void optimize_position(Position &position) {
    auto first_entry = find_if_not(begin(position.values), end(position.values), static_cast<bool (*)(double)>(std::isnan));
    if (first_entry == position.values.end()) {
        position.time = 0;
        position.values.resize(0);
        return;
    }
    auto last_nan = find_if_not(rbegin(position.values), rend(position.values), static_cast<bool (*)(double)>(std::isnan));
    position.time = static_cast<uint16_t>(distance(begin(position.values), first_entry));
    auto last_entry = begin(position.values);
    advance(last_entry, distance(begin(position.values), last_nan.base()));
    position.values = std::vector<float>(first_entry, last_entry);
}

void optimize_positions(std::unordered_map<int32_t, Position> &positions) {
    for (auto &entry : positions) {
        optimize_position(entry.second);
        for (auto &conditional_entry: entry.second.conditional_positions) {
            optimize_position(*conditional_entry.second);
        }
    }
}

#endif //SFVSIM_NATIVE_LOADER_POSITION_H
