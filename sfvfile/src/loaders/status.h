//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_STATUS_H
#define SFVSIM_NATIVE_LOADER_STATUS_H

#include "../../include/sfvfile/bac.h"

struct status_struct {
    uint32_t player_flags;
    uint32_t opponent_flags;
};

inline void load_status(Script &script, tick_header& tick, tick_flags& tf, status_struct& elt) {
    script.status.emplace_back(tick.tick_start, tick.tick_end, tf, elt.player_flags, elt.opponent_flags);
}

#endif //SFVSIM_NATIVE_LOADER_STATUS_H
