//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_LOADER_HITBOX_H
#define SFVSIM_NATIVE_LOADER_HITBOX_H

#include "box.h"
#include "../../include/sfvfile/bac.h"

struct hitbox_base : box_base {
    uint16_t hit_restriction;
    uint16_t flags;
    uint8_t group;
    uint8_t strength;
    uint8_t number_of_hits;
    uint8_t hit_level;

    uint8_t hit_type;
    uint8_t juggle_limit;
    uint8_t juggle_increase;
    uint8_t unknown_flags;

    int16_t effect_index;
    uint16_t hit_range;
    uint32_t unknown11;
};

struct hitbox_ver1 : hitbox_base {
    uint32_t unknown12;
};
struct hitbox_ver0 : hitbox_base {
    static const uint32_t unknown12;
};
const uint32_t hitbox_ver0::unknown12 = 0;

template<typename T>
inline void load_hitbox(Script &script, tick_header &tick_header, tick_flags& tf, T &elt) {
    script.hitboxes.emplace_back(tick_header, tf, elt);
}

#endif //SFVSIM_NATIVE_LOADER_HITBOX_H
