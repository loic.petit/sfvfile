//
// Created by WydD on 01/04/2018.
//

#ifndef SFVSIM_NATIVE_BCHEADER_H
#define SFVSIM_NATIVE_BCHEADER_H


#include <cstdint>

struct BCHeader {
    char filetype[4];
    uint32_t unknown1;
    uint16_t unknown2;
    uint16_t version;
};

void check_header(BCHeader& header, const std::string& type);

#endif //SFVSIM_NATIVE_BCHEADER_H
