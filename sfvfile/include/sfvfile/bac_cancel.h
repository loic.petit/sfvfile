//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_CANCEL_H
#define SFVSIM_NATIVE_BAC_CANCEL_H

#include "bac_time_element.h"

class Cancel: public TimeElement {
public:
    template<typename TF>
    Cancel(int32_t tick_start, int32_t tick_end, TF &tf, int32_t cancel_list, uint32_t type) :
            TimeElement(tick_start, tick_end, tf), cancel_list(cancel_list), type(type) {}

    int32_t cancel_list;
    uint32_t type;
};

#endif //SFVSIM_NATIVE_BAC_CANCEL_H
