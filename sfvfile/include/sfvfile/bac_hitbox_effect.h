//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_HITBOX_EFFECT_H
#define SFVSIM_NATIVE_BAC_HITBOX_EFFECT_H

#include <cstdint>

enum HitEffectType {
    HIT_STAND,
    HIT_CROUCH,
    HIT_AIR,
    HIT_UNKNOWN,
    HIT_UNKNOWN2,
    GUARD_STAND,
    GUARD_CROUCH,
    GUARD_AIR,
    GUARD_UNKNOWN,
    GUARD_UNKNOWN2,
    COUNTERHIT_STAND,
    COUNTERHIT_CROUCH,
    COUNTERHIT_AIR,
    COUNTERHIT_UNKNOWN,
    COUNTERHIT_UNKNOWN2,
    UNKNOWN_STAND,
    UNKNOWN_CROUCH,
    UNKNOWN_AIR,
    UNKNOWN_UNKNOWN,
    UNKNOWN_UNKNOWN2
};

class HitboxEffect : public noncopyable {
public:
    template<typename Elt>
    HitboxEffect(Elt &elt) :
            type(elt.type), index(elt.index), damage_type(elt.damage_type), damage(elt.damage), stun(elt.stun),
            vtimer(elt.vtimer), ex_attacker(elt.ex_attacker), ex_defender(elt.ex_defender), vgauge(elt.vgauge),
            hit_freeze_attacker(elt.hit_freeze_attacker), hit_freeze_defender(elt.hit_freeze_defender),
            fuzzy_effect(elt.fuzzy_effect), main_recovery_duration(elt.main_recovery_duration),
            knockdown_duration(elt.knockdown_duration), juggle_start(elt.juggle_start),
            damage_scaling_group(elt.damage_scaling_group), knock_back(elt.knock_back), fall_speed(elt.fall_speed),
            unused1(elt.unused1), unused2(elt.unused2), index9(elt.index9), index14(elt.index14), index18(elt.index18),
            index22(elt.index22), index23(elt.index23), index24(elt.index24), index25(elt.index25) {}

    int16_t type;
    int16_t index;
    int32_t damage_type;
    int16_t damage;
    int16_t stun;
    int16_t vtimer;
    int16_t ex_attacker;
    int16_t ex_defender;
    int32_t vgauge;
    int16_t hit_freeze_attacker;
    int16_t hit_freeze_defender;
    int16_t fuzzy_effect;
    int16_t main_recovery_duration;
    int16_t knockdown_duration;
    int16_t juggle_start;
    int16_t damage_scaling_group;
    float knock_back;
    float fall_speed;

    uint8_t unused1;
    uint8_t unused2;

    int16_t index9;
    int16_t index14;
    int8_t index18;
    int32_t index22;
    int32_t index23;
    int32_t index24;
    int32_t index25;
};

#endif //SFVSIM_NATIVE_BAC_HITBOX_EFFECT_H
