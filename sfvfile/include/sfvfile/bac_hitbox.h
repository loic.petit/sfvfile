//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_HITBOX_H
#define SFVSIM_NATIVE_BAC_HITBOX_H

#include "bac_box.h"

class Hitbox : public Box {
public:
    template<typename Tick, typename TF, typename Elt>
    Hitbox(Tick &tick, TF &tf, Elt &elt) :
            Box(tick, tf, elt), hit_restriction(elt.hit_restriction), flags(elt.flags), group(elt.group),
            strength(elt.strength), number_of_hits(elt.number_of_hits), hit_level(elt.hit_level),
            hit_type(elt.hit_type), juggle_limit(elt.juggle_limit), juggle_increase(elt.juggle_increase),
            unknown_flags(elt.unknown_flags), effect_index(elt.effect_index), hit_range(elt.hit_range),
            unknown11(elt.unknown11), unknown12(elt.unknown12) {}

    uint16_t hit_restriction;
    uint16_t flags;
    uint8_t group;
    uint8_t strength;
    uint8_t number_of_hits;
    uint8_t hit_level;

    uint8_t hit_type;
    uint8_t juggle_limit;
    uint8_t juggle_increase;
    uint8_t unknown_flags;

    int16_t effect_index;
    uint16_t hit_range;
    uint32_t unknown11;
    uint32_t unknown12;
public:
    inline std::unique_ptr<Hitbox> clone(float dx, float dy, bool side) {
        return clone_box(*this, dx, dy, side);
    }
};

#endif //SFVSIM_NATIVE_BAC_HITBOX_H
