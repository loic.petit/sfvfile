//
// Created by WydD on 01/04/2018.
//

#ifndef SFVSIM_NATIVE_BCM_H
#define SFVSIM_NATIVE_BCM_H


#include <array>
#include <vector>
#include <memory>
#include <string>
#include <optional>
#include <istream>
#include <map>
#include <utility>
#include "bcheader.h"
#include "util.h"

class InputPart: private noncopyable {
public:
    InputPart(uint16_t type, uint16_t buffer, uint16_t direction, uint16_t properties, uint16_t uk2, uint16_t uk3,
                  uint16_t uk4, uint16_t uk5) :
            type(type), buffer(buffer),
            direction(direction),
            properties(properties), uk2(uk2), uk3(uk3), uk4(uk4), uk5(uk5) {}

public:
    uint16_t type;
    uint16_t buffer;
    uint16_t direction;
    uint16_t properties;

    uint16_t uk2;
    uint16_t uk3;
    uint16_t uk4;
    uint16_t uk5;
};

class Charge: private noncopyable {
public:
    Charge(uint16_t direction, uint16_t properties, uint16_t frames, uint16_t flags, uint16_t index, uint16_t uk2,
               uint16_t uk3) :
            direction(direction), properties(properties), frames(frames), flags(flags), index(index), uk2(uk2),
            uk3(uk3) {}

    uint16_t direction;
    uint16_t properties;
    uint16_t frames;
    uint16_t flags;
    uint16_t index;
    uint16_t uk2;
    uint16_t uk3;
};

class Move: private noncopyable {
public:
    template<typename Elt>
    Move(uint16_t index, std::string name, Elt &elt) :
            index(index), name(name), input(elt.input), input_flags(elt.input_flags),
            input_motion_index(elt.input_motion_index), script_index(elt.script_index),
            position_restriction(elt.position_restriction), restriction_distance(elt.restriction_distance),
            projectile_limit(elt.projectile_limit), projectile_group(elt.projectile_group),
            bloody_garden(elt.bloody_garden), category(elt.category), subcategories(elt.subcategories),
            stance(elt.stance), ex_requires(elt.ex_requires), ex_consumes(elt.ex_consumes),
            vmeter_requires(elt.vmeter_requires), vmeter_consumes(elt.vmeter_consumes),
            vtrigger_activated(elt.vtrigger_activated), vtrigger_consumes(elt.vtrigger_consumes),
            specific_vtrigger_needed(elt.specific_vtrigger_needed),
            specific_vtrigger_selected(elt.specific_vtrigger_selected), unknown3(elt.unknown3), unknown7(elt.unknown7),
            unknown9(elt.unknown9), unknown10(elt.unknown10), unknown11(elt.unknown11), unknown17(elt.unknown17),
            unknown18(elt.unknown18), unknown19(elt.unknown19), unknown20(elt.unknown20), unknown21(elt.unknown21),
            unknown22(elt.unknown22), unknown23(elt.unknown23), unknown24(elt.unknown24), unknown25(elt.unknown25),
            unknown26(elt.unknown26), unknown27(elt.unknown27), unknown28(elt.unknown28) {}

public:
    uint16_t index;
    std::string name;
    uint16_t input;
    uint16_t input_flags;
    uint16_t input_motion_index;
    int16_t script_index;
    uint32_t position_restriction;
    float restriction_distance;
    uint8_t projectile_limit;
    uint8_t projectile_group;
    bool bloody_garden;
    uint8_t category;
    uint8_t subcategories;
    uint16_t stance;
    uint16_t ex_requires;
    uint16_t ex_consumes;
    uint16_t vmeter_requires;
    uint16_t vmeter_consumes;
    bool vtrigger_activated;
    uint16_t vtrigger_consumes;
    bool specific_vtrigger_needed;
    uint8_t specific_vtrigger_selected;

    uint32_t unknown3;
    uint16_t unknown7;
    uint16_t unknown9;
    uint16_t unknown10;
    uint16_t unknown11;
    uint32_t unknown17;
    uint32_t unknown18;
    uint32_t unknown19;
    float unknown20;
    float unknown21;
    uint32_t unknown22;
    uint32_t unknown23;
    uint32_t unknown24;
    uint32_t unknown25;
    uint16_t unknown26;
    uint16_t unknown27;
    uint32_t unknown28;
};

class CancelListEntry: private noncopyable {
public:
    CancelListEntry(uint16_t move_id, const std::array<uint8_t, 36> unknown_bytes,
                    std::unique_ptr<std::array<uint32_t, 2>> && optional_unknown_ints) :
            move_id(move_id),
            unknown_bytes(unknown_bytes),
            optional_unknown_ints(std::move(optional_unknown_ints)) {}

    uint16_t move_id;
    std::array<uint8_t, 36> unknown_bytes;
    std::unique_ptr<std::array<uint32_t, 2>> optional_unknown_ints;
};

class CancelList: private noncopyable {
public:
    CancelList(size_t length, uint32_t unknown1) : unknown1(unknown1) {
        moves.reserve(length);
    }

    std::vector<CancelListEntry> moves;
    uint32_t unknown1;
};

class BCM: private noncopyable {
public:
    friend std::istream &operator>>(std::istream &is, BCM &obj);
    void load_data(uint8_t* base);
    std::vector<Charge> charges;
    std::vector<Move> moves;
    std::vector<std::vector<std::vector<InputPart>>> inputs;
    std::vector<std::unique_ptr<CancelList>> cancel_lists;
    BCHeader header;
};


#endif //SFVSIM_NATIVE_BCM_H
