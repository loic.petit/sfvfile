//
// Created by WydD on 07/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_H
#define SFVSIM_NATIVE_BAC_H


#include <memory>
#include <unordered_map>
#include <istream>
#include "bcheader.h"
#include "bac_autocancel.h"
#include "bac_status.h"
#include "bac_force.h"
#include "bac_cancel.h"
#include "bac_command.h"
#include "bac_hitbox.h"
#include "bac_hurtbox.h"
#include "bac_pushbox.h"
#include "bac_position.h"
#include "bac_hitbox_effect.h"

class EffectHeader: private noncopyable {
public:
    EffectHeader(int16_t unknown12, int16_t unknown13, int16_t unknown14, int16_t unknown15, int16_t unknown16,
                 int16_t unknown17, float unknown18, int16_t unknown19, int16_t unknown20, int16_t unknown21,
                 int16_t unknown22) :
            unknown12(unknown12), unknown13(unknown13), unknown14(unknown14), unknown15(unknown15),
            unknown16(unknown16), unknown17(unknown17), unknown18(unknown18), unknown19(unknown19),
            unknown20(unknown20), unknown21(unknown21), unknown22(unknown22) {}
    int16_t unknown12;
    int16_t unknown13;
    int16_t unknown14;
    int16_t unknown15;
    int16_t unknown16;
    int16_t unknown17;
    float unknown18;
    int16_t unknown19;
    int16_t unknown20;
    int16_t unknown21;
    int16_t unknown22;
};


class CommandList: private noncopyable {

public:
    explicit CommandList(const std::list<Command *> &&commands) : commands(commands) {}

    inline bool contains(uint32_t category, uint16_t command) {
        return find_one(category, command) != nullptr;
    }

    Command* find_one(uint32_t category, uint16_t command) {
        for(auto && cmd : commands) {
            if (cmd->category == category && cmd->command == command)
                return cmd;
        }
        return nullptr;
    }

    std::list<Command*> get(uint32_t category, uint16_t command) {
        std::list<Command*> result{};
        for(auto && cmd : commands) {
            if (cmd->category == category && cmd->command == command) {
                result.push_back(cmd);
            }
        }
        return result;
    }

    inline size_t size() {
        return commands.size();
    }
private:
    std::list<Command*> commands;
};

class Script: private noncopyable {
public:
    Script(uint16_t index, std::string name, int32_t first_hitbox_frame, int32_t last_action_frame,
               int32_t interrupt_frame, uint32_t total_ticks, uint32_t execution_flags, float slide_vx, float slide_vy,
               float slide_vz, float slide_ax, float slide_ay, float slide_az, uint32_t flags, int32_t type_offset,
               uint32_t unk13, std::unique_ptr<EffectHeader> &&effect_header)
            : name(std::move(name)), first_hitbox_frame(first_hitbox_frame), last_action_frame(last_action_frame),
              interrupt_frame(interrupt_frame), total_ticks(total_ticks), execution_flags(execution_flags),
              slide_vx(slide_vx), slide_vy(slide_vy), slide_vz(slide_vz), slide_ax(slide_ax), slide_ay(slide_ay),
              slide_az(slide_az), flags(flags), unk13(unk13), effect_header(std::move(effect_header)), index(index),
              type_offset(type_offset) {}

public:
    uint16_t index;
    std::string name;
    int32_t first_hitbox_frame;
    int32_t last_action_frame;
    int32_t interrupt_frame;
    uint32_t total_ticks;

    uint32_t execution_flags;
    float slide_vx;
    float slide_vy;
    float slide_vz;
    float slide_ax;
    float slide_ay;
    float slide_az;
    uint32_t flags;
    int32_t type_offset;
    uint32_t unk13;

    std::unique_ptr<EffectHeader> effect_header;
    std::vector<AutoCancel> auto_cancels;
    std::vector<Status> status;
    std::vector<Force> forces;
    std::vector<Cancel> cancels;
    std::vector<Command> commands;
    std::vector<Hitbox> hitboxes;
    std::vector<Hurtbox> hurtboxes;
    std::vector<Pushbox> pushboxes;
    std::unordered_map<int32_t, Position> positions;

    inline std::list<AutoCancel*> get_auto_cancels_at(int32_t time, int32_t state) {
        return timeline_at(auto_cancels, time, state);
    }

    inline std::list<Status*> get_status_at(int32_t time, int32_t state) {
        return timeline_at(status, time, state);
    }

    inline std::list<Force*> get_forces_at(int32_t time, int32_t state) {
        return timeline_at(forces, time, state);
    }

    inline std::list<Cancel*> get_cancels_at(int32_t time, int32_t state) {
        return timeline_at(cancels, time, state);
    }

    inline std::unique_ptr<CommandList> get_commands_at(int32_t time, int32_t state) {
        return std::make_unique<CommandList>(timeline_at(commands, time, state));
    }

    inline std::list<std::unique_ptr<Hitbox>> get_hitboxes_at(int32_t time, int32_t state, float dx, float dy, bool side) {
        return timeline_clone_at(hitboxes, time, state, dx, dy, side);
    }

    inline std::list<std::unique_ptr<Hurtbox>> get_hurtboxes_at(int32_t time, int32_t state, float dx, float dy, bool side) {
        return timeline_clone_at(hurtboxes, time, state, dx, dy, side);
    }

    inline std::list<std::unique_ptr<Pushbox>> get_pushboxes_at(int32_t time, int32_t state, float dx, float dy, bool side) {
        return timeline_clone_at(pushboxes, time, state, dx, dy, side);
    }

    inline std::optional<float> get_position_shift_at(int32_t state, uint32_t category, int32_t time) {
        auto it = positions.find(category);
        if (it == positions.end()) {
            return {};
        }
        return it->second.at(state, static_cast<int16_t>(time));
    }
};

class ScriptTable: private noncopyable {
public:
    ScriptTable(int16_t unknown1) : unknown1(unknown1) {}

    int16_t unknown1;
    std::vector<std::unique_ptr<Script>> scripts;
};

class BAC: private noncopyable {
public:
    friend std::istream& operator>>(std::istream& is, BAC& obj);
    void load_data(uint8_t *base);
    BCHeader header;
    std::vector<ScriptTable> script_tables;
    std::vector<std::vector<HitboxEffect>> hitbox_effects;
};


#endif //SFVSIM_NATIVE_BAC_H
