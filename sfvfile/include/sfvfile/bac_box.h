//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_BOX_H
#define SFVSIM_NATIVE_BAC_BOX_H

#include "bac_time_element.h"

inline float range_intersection(float x1, float w1, float x2, float w2, bool side_to_consider) {
    float result;
    if (side_to_consider) {
        result = x1 + w1 - x2;
    } else {
        result = x2 + w2 - x1;
    }
    return result;
}

inline float range_intersection(float x1, float w1, float x2, float w2) {
    return range_intersection(x1, w1, x2, w2, x1 <= x2);
}

template<typename B>
std::unique_ptr<B> clone_box(const B &b, float dx, float dy, bool side) {
    std::unique_ptr<B> res = std::make_unique<B>(b, b, b);
    if (side) {
        res->x += dx;
    } else {
        res->x = dx - res->x - res->width;
    }
    res->y += dy;
    return res;
}

class Box : public TimeElement {
public:
    template<typename Tick, typename TF, typename Elt>
    Box(Tick &tick, TF &tf, Elt &elt) :
            TimeElement(tick.tick_start, tick.tick_end, tf), x(elt.x), y(elt.y), z(elt.z), width(elt.width),
            height(elt.height), unknown1(elt.unknown1), unknown2(elt.unknown2), unknown3(elt.unknown3), unknown4(elt.unknown4), unknown5(elt.unknown5) {}

    float x;
    float y;
    float z;
    float width;
    float height;

    uint32_t unknown1;
    uint16_t unknown2;
    uint16_t unknown3;
    uint16_t unknown4;
    uint16_t unknown5;
public:
    inline bool does_collide(const Box &box) {
        return range_intersection(x, width, box.x, box.width) >= 0 && range_intersection(y, height, box.y, box.height) >= 0;
    }
    inline float x_collision_distance(const Box& box, bool side) {
        return range_intersection(x, width, box.x, box.width, side);
    }

};

#endif //SFVSIM_NATIVE_BAC_BOX_H
