//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_COMMAND_H
#define SFVSIM_NATIVE_BAC_COMMAND_H

#include <utility>
#include <vector>
#include "bac_time_element.h"

class Command : public TimeElement {
public:
    template <typename TF>
    Command(int32_t tick_start, int32_t tick_end, TF &tf, uint32_t category, uint16_t command, uint32_t parameter_size) :
            TimeElement(tick_start, tick_end, tf), category(category), command(command), parameters(parameter_size) {}

    uint32_t category;
    uint16_t command;
    std::vector<int32_t> parameters;
};

#endif //SFVSIM_NATIVE_BAC_COMMAND_H
