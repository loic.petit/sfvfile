//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_HURTBOX_H
#define SFVSIM_NATIVE_BAC_HURTBOX_H

#include "bac_box.h"

class Hurtbox : public Box {
public:
    template<typename Tick, typename TF, typename Elt>
    Hurtbox(Tick &tick, TF &tf, Elt &elt) :
            Box(tick, tf, elt), vulnerability_flags(elt.vulnerability_flags), unknown7(elt.unknown7),
            armor_group(elt.armor_group), armor_strength(elt.armor_strength), armor_count(elt.armor_count),
            unknown9(elt.unknown9), hurt_type(elt.hurt_type), armor_effect(elt.armor_effect),
            hurt_level(elt.hurt_level), armor_freeze(elt.armor_freeze), unknown11(elt.unknown11),
            unknown12(elt.unknown12), unknown13(elt.unknown13) {}

    int16_t vulnerability_flags;
    int16_t unknown7;
    int8_t armor_group;
    int8_t armor_strength;
    int8_t armor_count;
    uint8_t unknown9;

    uint32_t hurt_type;

    int16_t armor_effect;
    int16_t hurt_level;
    int16_t armor_freeze;
    int16_t unknown11;

    float unknown12;
    uint32_t unknown13;
public:
    inline std::unique_ptr<Hurtbox> clone(float dx, float dy, bool side) {
        return clone_box(*this, dx, dy, side);
    }
};

#endif //SFVSIM_NATIVE_BAC_HURTBOX_H
