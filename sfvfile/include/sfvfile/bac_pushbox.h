//
// Created by WydD on 08/04/2018.
//

#ifndef SFVSIM_NATIVE_BAC_PUSHBOX_H
#define SFVSIM_NATIVE_BAC_PUSHBOX_H

#include "bac_box.h"

class Pushbox : public Box {
public:
    template<typename Tick, typename TF, typename Elt>
    Pushbox(Tick &tick, TF &tf, Elt &elt) :
            Box(tick, tf, elt), unknown6(elt.unknown6) {}

    uint32_t unknown6;
public:
    inline std::unique_ptr<Pushbox> clone(float dx, float dy, bool side) {
        return clone_box(*this, dx, dy, side);
    }
    void apply_correction(float correction) {
        this->x += correction;
    }
};

#endif //SFVSIM_NATIVE_BAC_PUSHBOX_H
