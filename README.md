# sfvfile
sfvfile is a C++ based project which is able to read the main gameplay uassets from Street Fighter V.

A python 3 binding is provided for easier experiments. The binding has been optimized to be as fast as possible without too much memory overhead.

This can be viewed as an alternative of the famous [MoveTool](https://github.com/lullius/MoveTool/) project with only the reading part.

## Native Usage
### Main objects
There are three main objects that you can use: `BAC`, `BCM`, `BCH`. Here's how you load them.
```cpp
BAC bac;
{
    std::ifstream file{"BAC_ALX.uasset", std::ios::binary | std::ios::in};
    file >> bac;
}
// Use the bac file 
```

After it's just a matter of exploiting the loaded object using its internal structure and methods.

_Memory concerns_: the main objects hold the memory rights, if you destroy those, every subsequent objects will be freed.

### Reading the objects
Most reading operations are simple attributes reads like:
```cpp
// reading the damage of HE number 152 in situation GUARD_STAND
bac.hitbox_effects[152][GUARD_STAND].damage
```

Check out the test folder to see some examples.

After that it's just a matter of knowing the object attributes and I strongly encourage you to look at the header files
and the [sfvsim wiki](https://gitlab.com/loic.petit/sfvsim/wikis/home) that explains how files are structured.

The `Script` object has however a couple of functions to ease up the exploration of the timeline.
```cpp
// self explanatory getters, pointers exists as long as the main object exist
std::list<AutoCancel*> get_auto_cancels_at(int32_t time)
std::list<Status*> get_status_at(int32_t time)
std::list<Force*> get_forces_at(int32_t time)
std::list<Cancel*> get_cancels_at(int32_t time)
// category is a flag (see wiki)
std::optional<float> get_position_shift_at(uint32_t category, int32_t time)
// the command list is a user-owned object that facilitate the exploration of commands using find_one, contains or find_all
std::unique_ptr<CommandList> get_commands_at(int32_t time)
// duplicate and shift hitboxes
std::list<std::unique_ptr<Hitbox>> get_hitboxes_at(int32_t time, float dx, float dy, bool side)
std::list<std::unique_ptr<Hurtbox>> get_hurtboxes_at(int32_t time, float dx, float dy, bool side)
std::list<std::unique_ptr<Pushbox>> get_pushboxes_at(int32_t time, float dx, float dy, bool side)
```

## Python bindings
### Installation
To install the python binding you will need to have:
* C++17 compiler
* Python 3
* CMake >= 3.1.0
* numpy installed in the target python runtime

If you have those then a simple `pip` will build and install everything in your environment:
```bash
pip install --upgrade .
```

The build environment has been prepared for Windows (with VS2015). It is possible to compile for Linux / OSX but that requires manual 
adjustments on the native binding CMake file, ask me if necessary.

### Usage
After installation (or manual build) you can do the following:
```
from sfvfile import BAC
bac = BAC("BAC_ALX.uasset")
print(bac.hitbox_effects[152][5].damage)
```

### I have segfault, what's happening?
Usually this is due to early memory release. Because python is just a view of the C++ object, if you get rid of the main
object reference, your object will be destroyed, hence all your old references point to delete objects.

I'm aware that this breaks conventions and frankly I don't care, I just want to get the best performance where it can be
 found.

### Python binding specifics
* Floats are `numpy.float32`. Because python deals floats as doubles, the python wrapper shouldn't return PyFloat
 objects. That is why, all float values are actually numpy.float32 which can be manipulated like any number object in 
 python but are stored in 32bits.
* All timeline elements support a `asdict()` method that provide a `dict` object in order to be textually serialized.
* All user-owned function results (such as `get_hitboxes_at`) are owned by Python and will be freed when Python feels
like it.
* The wrapping has been made using the native C extension API without any 3rd party library to have the lowest overhead
possible.

## Code quality warning
This project was meant to be an extended experiment with me around C++17 in order to get the most performance possible.

That is why you should find stuff that you may not like (C-style casts, single_ptr where shared_ptr could have worked etc.)
 but it works and it's fast.

## Usage
This is now the core data layer of the [sfvsim](https://gitlab.com/loic.petit/sfvsim/) project which aim to simulate
the engine of Street Fighter V. Check it out!

## Library Building
If you just install the python bindings, do it via pip. Otherwise, to build the lib layer by itself you just need a C++17 compiler and CMake. 

If you want to build the python binding as well you need to provide the PYTHON_ROOT variable.

## Author
Loïc _WydD_ Petit [@WydD](https://twitter.com/WydD) [/u/-WydD-](https://reddit.com/u/-WydD-)

## Licence
The project code is public and under the [MIT Licence](LICENSE) 

## Thanks
This project was made possible because of the [MoveTool](https://github.com/lullius/MoveTool/) project. So thanks to 
lullius and all the past contributors.
