#include <iostream>
#include <string>
#include <fstream>
#include "sfvfile/bch.h"
#include "sfvfile/bcm.h"
#include "sfvfile/bac.h"
#include "../sfvfile/src/util/ioutil.h"
#include "sfvfile/uasset.h"

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("Missing argument\n");
        exit(1);
    }
    {
        Uasset uasset;
        std::ifstream file{argv[1], std::ios_base::in | std::ios::binary };
        file >> uasset;

        {
            uasset.data["fully_parsed"] = uasset.fully_parsed;
            std::ofstream o(std::string(argv[1]) + ".json");
            o << std::setw(2) << uasset.data << std::endl;
        }
    }
/*
    BCH bch;
    {
        std::ifstream file{"BCH_CMY.uasset", std::ios::binary | std::ios::in};
        file >> bch;
    }
    for (auto it : bch.content) {
        std::cout << it.first << " : " << it.second << std::endl;
    }

    BCM bcm;
    {
        std::ifstream file{"BCM_VEG.uasset", std::ios::binary | std::ios::in};
        file >> bcm;
    }

    BAC bac;
    {
        std::ifstream file{"BAC_ALX.uasset", std::ios::binary | std::ios::in};
        file >> bac;
    }
    printf("%d\n", bac.hitbox_effects[152][GUARD_STAND].damage);
    ScriptTable &x = bac.script_tables[0];
    {
        auto list = x.scripts[804]->get_commands_at(0, 0);
        const auto pCommand = list->find_one(0, 11);
        printf("%d\n", pCommand->parameters.size());
    }

    {
        auto list = x.scripts[804]->get_commands_at(0, 0);
        const auto pCommand = list->find_one(0, 11);
        printf("%d\n", pCommand->parameters.size());
    }

    BAC bac_eff_fail;
    {
        std::ifstream file{"BAC_CMY_eff.uasset", std::ios::binary | std::ios::in};
        file >> bac_eff_fail;
    }

    BAC bac_eff_success;
    {
        std::ifstream file{"BAC_URN_eff.uasset", std::ios::binary | std::ios::in};
        file >> bac_eff_success;
    }
    BAC bac_z31;
    {
        std::ifstream file{"BAC_Z24.uasset", std::ios::binary | std::ios::in};
        file >> bac_z31;
        printf("Wokay");
    }*/
    return 0;
}